from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from polls.forms import PollVoteForm
from polls.models import Poll, Choice

def home(request):
    content = ''
    for poll in Poll.objects.all():
        content += poll.question
    context = {'polls': Poll.objects.all()}

    return render(request, 'polls/home.html', context)

def poll(request, poll_id):
    if request.method == 'POST':
        choice = Choice.objects.get(id=request.POST['vote'])
        choice.votes += 1
        choice.save()
        return HttpResponseRedirect(reverse('polls.views.poll', args=[poll_id,]))

    poll = Poll.objects.get(pk=poll_id)
    form = PollVoteForm(poll=poll)
    return render(request, 'polls/poll.html', {'poll': poll, 'form': form})
